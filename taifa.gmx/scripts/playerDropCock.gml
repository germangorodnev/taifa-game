cockToCarry.y = y;
with (cockToCarry) {
    // check the zone
    if (place_meeting(x, y, oDropZone)  
        && state == COCK_STATE.__CARRIED) {
        cockSetState(COCK_STATE.__CAUGHT);
        global.points += 10;
    } else
        cockSetState(COCK_STATE.__IDLE);
}
cockToCarry = noone;
anMaxAngle = 6;
anAngleStep = 1.5;
