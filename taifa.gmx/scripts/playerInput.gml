for (var i = 0; i < KEYS.__LEN; ++i) {
    keys[i] = 0;
}

if (keyboard_check(ord('A'))) {
    keys[KEYS.LEFT] = 1;
}
if (keyboard_check(ord('D'))) {
    keys[KEYS.RIGHT] = 1;
}
if (keyboard_check(ord('W'))) {
    keys[KEYS.UP] = 1;
}
if (keyboard_check(ord('S'))) {
    keys[KEYS.DOWN] = 1;
}
if (keyboard_check_pressed(vk_space)) {
    keys[KEYS.PICKUP] = 1;
}
