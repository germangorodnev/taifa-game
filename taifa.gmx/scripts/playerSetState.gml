///playerSetState(new state)

var prevState = state;
state = argument[0];

switch (state) {
    case PLAYER_STATE.__IDLE:
        setAnim(sPlayerIdle, 0);
        break;
    case PLAYER_STATE.__WALK:
        setAnim(sPlayerIdle, 0);
        break;
    case PLAYER_STATE.__IDLE_COCK:
        setAnim(sPlayerPickup, 0);
        if (argument_count > 1)
            playerSetCockToCarry(argument[1]);
        break;
    case PLAYER_STATE.__WALK_COCK:
        setAnim(sPlayerPickup, 0);
        if (argument_count > 1)
            playerSetCockToCarry(argument[1]);
        break;
}
