///setAnim(anim, restart)
var anim = argument[0];
if (!argument[1]) {
    // need to check its not set already
    if (sprite_index != anim) {
        sprite_index = anim;
        image_index = 9;
    }
} else {
    sprite_index = anim;
}
